import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit, OnDestroy {
  @Output() closeSideNav = new EventEmitter<void>();

  isLogged: boolean;

  private subscription: Subscription;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.subscription = this.authService.authChange.subscribe(
      value => {
        this.isLogged = value;
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  close() {
    this.closeSideNav.emit();
  }

  onLogout() {
    this.authService.logout();
    this.close();
  }

}
