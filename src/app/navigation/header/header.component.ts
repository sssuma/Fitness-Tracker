import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Output() toggleSideNav = new EventEmitter<void>();
  isLogged: boolean;

  private subscription: Subscription;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.subscription = this.authService.authChange.subscribe(
      value => {
        this.isLogged = value;
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onToggle() {
    this.toggleSideNav.emit();
  }

  onLogout(){
    this.authService.logout();
  }

}
