import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {TrainingService} from './training.service';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit, OnDestroy {
  ongoingTraining = false;

  private runningExerciseSubscription: Subscription;

  constructor(private trainingService: TrainingService) {
  }

  ngOnInit() {
    this.runningExerciseSubscription = this.trainingService.runningExerciseChange.subscribe(
      exercise => exercise ? this.onTrainingStart() : this.onTrainingExit()
    );
  }

  ngOnDestroy() {
    this.runningExerciseSubscription.unsubscribe();
  }

  onTrainingStart() {
    this.ongoingTraining = true;
  }

  onTrainingExit() {
    this.ongoingTraining = false;
  }
}
