// Shouldn't a exercise contain only an ID and a Name?
// And the remaining properties be defined in a Training interface, that have a exercise?

export interface Exercise {
  id: string;
  name: string;
  duration: number;
  caloriesBurned: number;
  date?: Date;
  state?: 'completed' | 'cancelled' | null;
}
