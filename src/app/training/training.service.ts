import {Injectable} from '@angular/core';
import {Exercise} from './exercise.model';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class TrainingService {
  runningExerciseChange = new Subject<Exercise>();

  private availableExercises: Exercise[];
  private runningExercise: Exercise;

  constructor() {
    this.init();
  }

  setAvailableExercises(): void {
    this.availableExercises = [
      {id: 'crunches', name: 'Crunches', duration: 30, caloriesBurned: 8},
      {id: 'touch-toes', name: 'Touch Toes', duration: 180, caloriesBurned: 15},
      {id: 'side-lunges', name: 'Side Lunges', duration: 120, caloriesBurned: 18},
      {id: 'burpees', name: 'Burpees', duration: 60, caloriesBurned: 8}
    ];
  }

  getAvailableExercises(): Exercise[] {
    return this.availableExercises.slice();
  }

  startExercise(id: string): void {
    this.runningExercise = this.availableExercises.find(exercise => exercise.id === id);
    this.updateRunningExercise();
  }

  getRunningExercise() {
    return this.runningExercise ? {...this.runningExercise} : null;
  }

  stopRunningExercise() {
    this.runningExercise = null;
    this.updateRunningExercise();
  }

  private updateRunningExercise() {
    this.runningExerciseChange.next(this.getRunningExercise());
  }

  private init(): void {
    this.setAvailableExercises();
  }

}
