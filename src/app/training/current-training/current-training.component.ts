import {Component, OnInit} from '@angular/core';
import {StopTrainingComponent} from './stop-training/stop-training.component';
import {MatDialog} from '@angular/material';
import {TrainingService} from '../training.service';
import {Exercise} from '../exercise.model';

@Component({
  selector: 'app-current-training',
  templateUrl: './current-training.component.html',
  styleUrls: ['./current-training.component.css']
})
export class CurrentTrainingComponent implements OnInit {
  exercise: Exercise;
  progress: number;
  progressLabel: string;
  timerID: number;
  timeElapsed: number;
  timeElapsedLabel: string;

  constructor(private dialog: MatDialog,
              private trainingService: TrainingService) {
  }

  ngOnInit() {
    this.timeElapsed = 0;
    this.exercise = this.trainingService.getRunningExercise();
    this.runTimer();
  }

  onTrainingStop() {
    this.stopInterval();
    this.openStopDialog();
  }

  private openStopDialog() {
    const dialogData = {
      data: {
        progress: this.progressLabel
      }
    };
    const stopDialog = this.dialog.open(StopTrainingComponent, dialogData);

    stopDialog.afterClosed().subscribe(result => {
      if (result) {
        this.finishTraining();
      } else {
        this.runTimer();
      }
    });
  }

  private finishTraining() {
    this.trainingService.stopRunningExercise();
  }

  private runTimer() {
    this.timerID = setInterval(() => {
      this.timeElapsed = this.timeElapsed + 0.1;
      this.progress = (this.timeElapsed) / this.exercise.duration * 100;

      this.progressLabel = (this.progress).toFixed(1);
      this.timeElapsedLabel = (this.timeElapsed).toFixed(1);

      if (this.timeElapsed >= this.exercise.duration) {
        this.stopInterval();
      }
    }, 100);
  }

  private stopInterval() {
    clearInterval(this.timerID);
  }
}
