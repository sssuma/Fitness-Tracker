import {Component, OnDestroy, OnInit} from '@angular/core';
import {TrainingService} from '../training.service';
import {Subscription} from 'rxjs/Subscription';
import {Exercise} from '../exercise.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit, OnDestroy {
  exercises: Exercise[];
  selectedExerciseId: string;

  // private availableTrainingSubscription: Subscription;

  constructor(private trainingService: TrainingService) {
  }

  ngOnInit() {
    this.loadExercises();
  }

  ngOnDestroy() {
    // this.availableTrainingSubscription.unsubscribe();
  }

  loadExercises() {
    // this.availableTrainingSubscription = this.trainingService.getAvailableExercises().subscribe(
    //   exercises => {
    //     this.exercises = exercises;
    //   }
    // );
    this.exercises = this.trainingService.getAvailableExercises();
  }

  onNewTraining(form: NgForm) {
    const id = form.value.selectedExerciseId;
    this.trainingService.startExercise(id);
  }
}
