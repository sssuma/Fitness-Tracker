import {Subject} from 'rxjs/Subject';

import {Injectable} from '@angular/core';
import {User} from './user.model';
import {AuthData} from './auth-data.model';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {
  authChange = new Subject<boolean>();

  private user: User;

  constructor(private router: Router) {
  }

  login(authData: AuthData) {
    this.setUser(authData);
    this.updateAuthChange();
    this.navigateToTraining();
  }

  signup(authData: AuthData) {
    this.setUser(authData);
    this.updateAuthChange();
    this.navigateToTraining();
  }

  logout() {
    this.user = null;
    this.updateAuthChange();
    this.nagivateToHome();
  }

  getUser() {
    return {
      ...this.user
    };
  }

  isAuth() {
    return this.user != null;
  }

  private setUser(authData: AuthData) {
    this.user = {
      email: authData.email,
      userId: Math.round(Math.random() * 10000).toString()
    };
  }

  private updateAuthChange() {
    this.authChange.next(this.isAuth());
  }

  private navigateToTraining() {
    this.router.navigate(['/training']);
  }

  private nagivateToHome() {
    this.router.navigate(['/']);

  }

}
